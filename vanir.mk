#Squisher Choosing
DHO_VENDOR :=vanir

# Boot animation
PRODUCT_COPY_FILES += \
    vendor/vanir/proprietary/boot_animations/720x1280.zip:system/media/bootanimation.zip

# Release name
PRODUCT_RELEASE_NAME := p880

# Vanir configuration
$(call inherit-product, vendor/vanir/products/common_phones.mk)

# This device has NFC
$(call inherit-product, vendor/vanir/config/nfc_enhanced.mk)

# Inherit device configuration
$(call inherit-product, device/lge/p880/device.mk)

# CM Snap camera
PRODUCT_PACKAGES += Snap

# Extra tools in CM
PRODUCT_PACKAGES += \
    bash \
    bzip2 \
    curl \
    unrar \
    unzip \
    zip

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := p880
PRODUCT_NAME := vanir_p880
PRODUCT_BRAND := LG
PRODUCT_MODEL := LG-P880
PRODUCT_MANUFACTURER := LGE
PRODUCT_RESTRICT_VENDOR_FILES := false

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=x3 BUILD_FINGERPRINT=lge/x3_open_eu/x3:4.1.2/JZO54K/P88020c.20d4ae5fac:user/release-keys PRIVATE_BUILD_DESC="x3_open_eu-user 4.1.2 JZO54K P88020c.20d4ae5fac release-keys"

# Enable Torch
PRODUCT_PACKAGES += Torch